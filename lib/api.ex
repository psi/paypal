defmodule PayPal.API do
  @moduledoc """
  Documentation for PayPal.API. This module is about the base HTTP functionality
  """
  @base_url_sandbox "https://api.sandbox.paypal.com/v1/"
  @base_url_live "https://api.paypal.com/v1/"

  @doc """
  Requests an OAuth token from PayPal, returns a tuple containing the token and seconds till expiry.
  Note: If your name is not Zen and you're reading this, unless you're sending me a PR (thanks!), you probably don't need this.
  Possible returns:
  - {:ok, {"XXXXXXXXXXXXXX", 32000}}
  - {:error, :unauthorised}
  - {:error, :bad_network}
  ## Examples
    iex> PayPal.API.get_access_token
    {:ok, {"XXXXXXXXXXXXXX", 32000}}
  """
  @spec get_access_token(String.t(), String.t()) :: {atom, any}
  def get_access_token(client_id, client_secret) do
    headers = %{
      "Content-Type" => "application/x-www-form-urlencoded"
    }

    options = [hackney: [basic_auth: {client_id, client_secret}]]
    form = {:form, [grant_type: "client_credentials"]}

    (base_url() <> "oauth2/token")
    |> HTTPoison.post(form, headers, options)
    |> parse_resp()
  end

  @doc """
  Make a HTTP GET request to the correct API depending on environment, adding needed auth header.
  Note: If your name is not Zen and you're reading this, unless you're sending me a PR (thanks!), you probably don't need this.
  Possible returns:
  - {:ok, data}
  - {:ok, :not_found}
  - {:ok, :no_content}
  - {:error, :bad_network}
  - {:error, reason}
  ## Examples
    iex> PayPal.API.get(url)
    {:ok, {"XXXXXXXXXXXXXX", 32000}}
  """
  @spec get(String.t(), String.t()) :: {atom, any}
  def get(url, access_token) do
    headers = build_headers(access_token)

    (base_url() <> url)
    |> HTTPoison.get(headers)
    |> parse_resp()
  end

  @doc """
  Make a HTTP POST request to the correct API depending on environment, adding needed auth header.
  Note: If your name is not Zen and you're reading this, unless you're sending me a PR (thanks!), you probably don't need this.
  Possible returns:
  - {:ok, data}
  - {:ok, :not_found}
  - {:ok, :no_content}
  - {:error, :bad_network}
  - {:error, reason}
  ## Examples
    iex> PayPal.API.post(url, data, access_token)
    {:ok, {"XXXXXXXXXXXXXX", 32000}}
  """
  @spec post(String.t(), map, String.t()) :: {atom, any}
  def post(url, data, access_token) do
    {:ok, data} = Jason.encode(data)
    headers = build_headers(access_token)

    (base_url() <> url)
    |> HTTPoison.post(data, headers)
    |> parse_resp()
  end

  @doc """
  Make a HTTP PATCH request to the correct API depending on environment, adding needed auth header.
  Note: If your name is not Zen and you're reading this, unless you're sending me a PR (thanks!), you probably don't need this.
  Possible returns:
  - {:ok, data}
  - {:ok, :not_found}
  - {:ok, :no_content}
  - {:error, :bad_network}
  - {:error, reason}
  ## Examples
    iex> PayPal.API.patch(url, data, access_token)
    {:ok, {"XXXXXXXXXXXXXX", 32000}}
  """
  @spec patch(String.t(), map, String.t()) :: {atom, any}
  def patch(url, data, access_token) do
    {:ok, data} = Jason.encode(data)
    headers = build_headers(access_token)

    (base_url() <> url)
    |> HTTPoison.patch(data, headers)
    |> parse_resp()
  end

  def parse_resp(resp) do

    case resp do
      {:ok, %{status_code: 401}} ->
        {:error, :unauthorised}

      {:ok, %{body: body, status_code: 200}} ->
        {:ok, Jason.decode!(body, keys: :atoms)}

      {:ok, %{status_code: 200}} ->
        {:ok, nil}

      {:ok, %{body: body, status_code: 201}} ->
        {:ok, Jason.decode!(body, keys: :atoms)}

      {:ok, %{status_code: 404}} ->
        {:ok, :not_found}

      {:ok, %{status_code: 204}} ->
        {:ok, :no_content}
      {:ok, %{body: body, status_code: 400}} ->
        {:error, Jason.decode!(body, keys: :atoms)}

      {:ok, %{status_code: 400}} ->
        {:error, :malformed_request}

      {:ok, %{body: body}} ->
        IO.inspect(resp)
        {:error, body}

      _ ->
        {:error, :bad_network}
    end
  end

  @spec build_headers(String.t()) :: map
  defp build_headers(access_token) do
    %{"Authorization" => "Bearer #{access_token}", "Content-Type" => "application/json"}
  end

  @spec base_url :: String.t()
  defp base_url do
    case PayPal.is_livemode() do
      true ->
        @base_url_live

      _ ->
        @base_url_sandbox
    end
  end
end
