defmodule PayPal do
  alias PayPal.API

  @moduledoc """
  Documentation for PayPal.
  """

  @doc """
  Hello world.

  ## Examples

      iex> PayPal.hello()
      :world

  """

  def get_access_token do
    appid = get_client_id()
    secret = get_client_secret()
    API.get_access_token(appid, secret)
  end

  def get_client_id do
    Application.get_env(:pay_pal, :client)
    |> Keyword.get(:id)
  end

  def get_client_secret do
    Application.get_env(:pay_pal, :client)
    |> Keyword.get(:secret)
  end

  def is_livemode do
    Application.get_env(:pay_pal, :livemode)
  end
end
